include '../SwiftApi/SwiftApi.thrift'

namespace java org.radic.minecraft.swiftapi.example.thrift
namespace csharp org.radic.minecraft.swiftapi.example.thrift
namespace php Radic.Minecraft.SwiftApi.Example.Thrift

struct ExampleStruct {
    1:string name,
    2:bool enabled
}


service SwiftApiExample {
    ExampleStruct getData(1:string authString) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool setData(1:string authString, 2:string name, 3:bool enabled) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
}