package org.radic.minecraft.swiftapi.example.handlers;

import org.apache.thrift.TException;
import org.phybros.minecraft.extensions.SwiftApiHandler;
import org.phybros.thrift.EAuthException;
import org.phybros.thrift.EDataException;
import org.radic.minecraft.swiftapi.example.thrift.ExampleStruct;
import org.radic.minecraft.swiftapi.example.thrift.SwiftApiExample;

/**
 * Created by radic on 9/1/14.
 */
public class ExampleApiHandler extends SwiftApiHandler implements SwiftApiExample.Iface
{

    @Override
    public ExampleStruct getData(String authString) throws EAuthException, EDataException, TException
    {
        return null;
    }

    @Override
    public boolean setData(String authString, String name, boolean enabled) throws EAuthException, EDataException, TException
    {
        return false;
    }
}
