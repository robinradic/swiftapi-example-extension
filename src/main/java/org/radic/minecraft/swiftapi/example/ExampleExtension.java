package org.radic.minecraft.swiftapi.example;

import org.apache.thrift.TProcessor;
import org.phybros.minecraft.extensions.SwiftExtension;
import org.radic.minecraft.swiftapi.example.handlers.ExampleApiHandler;
import org.radic.minecraft.swiftapi.example.thrift.SwiftApiExample;

/**
 * Created by radic on 9/1/14.
 */
public class ExampleExtension extends SwiftExtension
{
    @Override
    public void register()
    {
        registerApiHandler("org.radic.minecraft.swiftapi.example.thrift.SwiftApiExample");
        registerConfig("example", "config")
                .string("name")
                .bool("enabled");
        registerConfig("example", "otherconf")
                .string("othername")
                .bool("otherenabled");
    }

    @Override
    public TProcessor getApiProcessor(String name)
    {
        if(name.equals("SwiftApiExample")){
            return new SwiftApiExample.Processor(new ExampleApiHandler());
        }
        return null;
    }

    @Override
    public void enable()
    {
        boolean enabled = config("config").getBoolean("enabled");
        boolean otherenabled = config("otherconf").getBoolean("otherenabled");
    }

    @Override
    public void disable()
    {

    }
}
